#!/bin/bash

export ZSH_LOAD_PACKAGED_KUBECTL_COMMANDS=true
export ZSH_KUBECTL_CUSTOM_PLUGINS_DIR=$(dirname "$0")
COMPLETION_FILE_PATH="${ZSH_KUBECTL_CUSTOM_PLUGINS_DIR}/kubectl-custom-completion"
export ZSH_KUBECTL_COMPLETION_MODEL='bash'


${ZSH_KUBECTL_CUSTOM_PLUGINS_DIR}/scripts/add-kubectl-plugins.sh

echo "alias update_kubectl_custom_commands='cd $ZSH_KUBECTL_CUSTOM_PLUGINS_DIR; git pull --rebase; git submodule update --remote --merge; cd - > /dev/null'"

# This command is used a LOT both below and in daily life


# echo "alias k='kubectl'"

# Apply a YML file
echo "alias kaf='kubectl apply -f'"

# Drop into an interactive terminal on a container
echo "alias keti='kubectl exec -ti'"

# Manage configuration quickly to switch contexts between local, dev ad staging.
echo "alias kcuc='kubectl config use-context'"
echo "alias kcsc='kubectl config set-context'"
echo "alias kcdc='kubectl config delete-context'"
echo "alias kccc='kubectl config current-context'"

# General aliases
echo "alias kdel='kubectl delete'"
echo "alias kdelf='kubectl delete -f'"

# Pod management.
echo "alias kgp='kubectl get pods'"
echo "alias kgpw='kgp --watch'"
echo "alias kgpwide='kgp -o wide'"
echo "alias kep='kubectl edit pods'"
echo "alias kdp='kubectl describe pods'"
echo "alias kdelp='kubectl delete pods'"

# get pod by label: kgpl "app=myapp" -n myns
echo "alias kgpl='kgp -l'"

# Service management.
echo "alias kgs='kubectl get svc'"
echo "alias kgsw='kgs --watch'"
echo "alias kgswide='kgs -o wide'"
echo "alias kes='kubectl edit svc'"
echo "alias kds='kubectl describe svc'"
echo "alias kdels='kubectl delete svc'"

# Ingress management
echo "alias kgi='kubectl get ingress'"
echo "alias kei='kubectl edit ingress'"
echo "alias kdi='kubectl describe ingress'"
echo "alias kdeli='kubectl delete ingress'"

# Namespace management
echo "alias kgns='kubectl get namespaces'"
echo "alias kens='kubectl edit namespace'"
echo "alias kdns='kubectl describe namespace'"
echo "alias kdelns='kubectl delete namespace'"

# ConfigMap management
echo "alias kgcm='kubectl get configmaps'"
echo "alias kecm='kubectl edit configmap'"
echo "alias kdcm='kubectl describe configmap'"
echo "alias kdelcm='kubectl delete configmap'"

# Secret management
echo "alias kgsec='kubectl get secret'"
echo "alias kdsec='kubectl describe secret'"
echo "alias kdelsec='kubectl delete secret'"

# Deployment management.
echo "alias kgd='kubectl get deployment'"
echo "alias kgdw='kgd --watch'"
echo "alias kgdwide='kgd -o wide'"
echo "alias ked='kubectl edit deployment'"
echo "alias kdd='kubectl describe deployment'"
echo "alias kdeld='kubectl delete deployment'"
echo "alias ksd='kubectl scale deployment'"
echo "alias krsd='kubectl rollout status deployment'"

# Rollout management.
echo "alias kgrs='kubectl get rs'"
echo "alias krh='kubectl rollout history'"
echo "alias kru='kubectl rollout undo'"

# Port forwarding
echo "alias kpf='kubectl port-forward'"

# Tools for accessing all information
echo "alias kga='kubectl get all'"
echo "alias kgaa='kubectl get all --all-namespaces'"

# Logs
echo "alias kl='kubectl logs'"
echo "alias klf='kubectl logs -f'"

# File copy
echo "alias kcp='kubectl cp'"

# Node Management
echo "alias kgno='kubectl get nodes'"
echo "alias keno='kubectl edit node'"
echo "alias kdno='kubectl describe node'"
echo "alias kdelno='kubectl delete node'"
cat $COMPLETION_FILE_PATH


