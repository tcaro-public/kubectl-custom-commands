#!/bin/bash

# Placeholders list to remove at the end
declare -a placeholders_to_remove=(" CUSTOM_RESOURCE" " CUSTOM_POD" " CUSTOM_NODE" " CUSTOM_CONTEXT" " CUSTOM_CLUSTER" "FUNCTION_NAME" "KUBECTL_COMMAND" "CUSTOM_FUNCTION_HERE" "    CUSTOM_COMMAND_DECLARATION" )

PLUGIN_DIR="$ZSH_KUBECTL_CUSTOM_PLUGINS_DIR"
PLUGIN_COMPLETION_DIR="$PLUGIN_DIR/completions"
PLUGIN_TEMP_DIR="$PLUGIN_DIR/TEMP"

FILES=$PLUGIN_DIR/commands/*/*
TEMP_FILE_PATH="$PLUGIN_TEMP_DIR/model-temp.txt"


# Extract file name from path
# - Remove tree
# - Remove extension
# - Remove completion information
get_file_name() {
	
	basename=$(basename $1)
	
	filename="${basename%_completion-*}"
	filename="${filename%.*}"

	echo $filename

	unset basename
	unset filename
}

# Extract completion wanted from path
# - Return empty if none
get_completion_type() {
	basename=$(basename $1)

	completion_type="none"

	if [[ $basename == *"_completion-"* ]]; then

		completion_type="${basename##*_}"
		completion_type="${completion_type%.*}"
	fi

	echo $completion_type

	unset basename
	unset completion_type
}

# Remove all symlinks with pattern kubectl-
remove_previous_symlinks() {

	rm $HOME/bin/kubectl-* 2> /dev/null
}

# Create Symbolic Link of command
# - Make command executable
# - Create it in $HOME/bin (to avoid sudo restriction)
create_symlink() {

	filename=$(get_file_name $1)
	chmod +x $f
	ln -sf $f $HOME/bin/$filename

	unset filename
}

# Add command to custom kubectl completion file
# - Call external script (./completions/completion.engine.sh)
append_completion() {

	filename=$(get_file_name $1)
	filename=$(replace_placeholder "$filename" "_" "-")
	completion_type=$(get_completion_type $1)

	$PLUGIN_DIR/scripts/completion-engine.sh $filename $completion_type

	unset filename
	unset completion_type
}

# Remove placeholder in custom kubectl completion file
replace_placeholder() {
	
	content=$1
	placeholder=$2
	value=$3

	echo "${content//$placeholder/$value}"

	unset content
	unset placeholder
	unset value
}

# Step final to generate completion file
# - Remove placeholder
# - Put file in plugin directory
# - Remove temporary folder
finalize_init() {
	if [ -f $TEMP_FILE_PATH ]; then
		
		model="$(cat $TEMP_FILE_PATH)"

		for placeholder in "${placeholders_to_remove[@]}"
		do 	
		 	model=$(replace_placeholder "$model" "$placeholder" "")
		done

		echo -e "$model" > $PLUGIN_DIR/kubectl-custom-completion
	fi

	# Remove TEMP directory
	rm -rf $PLUGIN_TEMP_DIR

	# DELETE Broken symlinks
	find -L $HOME/bin -maxdepth 1 -type l -delete  

	unset model
}

main() {

	remove_previous_symlinks
	
	if [[ "$ZSH_LOAD_PACKAGED_KUBECTL_COMMANDS" = true ]]; 
	then
		# For each commands in plugin
		# - Create symlink to be discovered by kubectl
		# - Add custom completion in generated k completion file
		for f in $FILES
		do

			filename=$(get_file_name $f)

			if [[ $filename == "kubectl-"* ]]; then

				create_symlink $f
				append_completion $f
			fi
		done

		# Clean script
		# - Generate kubectl completion file
		# - Remove temporary folder
		# - Delete broken 
		finalize_init
	fi
}

main