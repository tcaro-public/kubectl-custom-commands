#!/bin/bash

PLUGIN_DIR="$ZSH_KUBECTL_CUSTOM_PLUGINS_DIR"
PLUGIN_COMPLETION_DIR="$PLUGIN_DIR/completions"
PLUGIN_TEMP_DIR="$PLUGIN_DIR/TEMP"
TEMP_FILE_PATH="$PLUGIN_TEMP_DIR/model-temp.txt"

COMPLETION_RESOURCE="completion-resource"
COMPLETION_POD="completion-pod"
COMPLETION_NODE="completion-node"
COMPLETION_CONTEXT="completion-context"
COMPLETION_CLUSTER="completion-cluster"


CUSTOM_RESOURCE="CUSTOM_RESOURCE"
CUSTOM_POD="CUSTOM_POD"
CUSTOM_NODE="CUSTOM_NODE"
CUSTOM_CONTEXT="CUSTOM_CONTEXT"
CUSTOM_CLUSTER="CUSTOM_CLUSTER"


FUNCTION_PLACEHOLDER="FUNCTION_NAME"
KUBECTL_CMD_PLACEHOLDER="KUBECTL_COMMAND"

CUSTOM_CMD_PLACEHOLDER="CUSTOM_FUNCTION_HERE"
CUSTOM_CMD_DECLARATION="    CUSTOM_COMMAND_DECLARATION"

# Load completion resource file
# - One by k8s resource available for completion
load_resource_template() {
	
	completion_type=$1
	file_name=''

	case "$completion_type" in
		$COMPLETION_POD)
			file_name="pod.txt" ;;
		$COMPLETION_NODE)
			file_name="node.txt" ;;
		$COMPLETION_CONTEXT)
			file_name="context.txt" ;;
		$COMPLETION_CLUSTER)
			file_name="cluster.txt" ;;
		*)
			file_name="resource.txt" ;;
		
	esac
	
	cat "$PLUGIN_COMPLETION_DIR/templates/$file_name"

	unset completion_type
	unset file_name
}

# Return resource placeholder
# - Used to add command in completion switch case
find_k8s_resource_placeholder() {
	
	completion_type=$1
	placeholder=''

	case "$completion_type" in
		$COMPLETION_POD)
			placeholder="$CUSTOM_POD" ;;
		$COMPLETION_NODE)
			placeholder="$CUSTOM_NODE" ;;
		$COMPLETION_CONTEXT)
			placeholder="$CUSTOM_CONTEXT" ;;
		$COMPLETION_CLUSTER)
			placeholder="$CUSTOM_CLUSTER" ;;
		*)
			placeholder="$CUSTOM_RESOURCE" ;;
		
	esac
	
	echo "$placeholder"

	unset completion_type
	unset placeholder
}

# Simple function to replace substring by an other one
replace_placeholder() {
	
	content=$1
	placeholder=$2
	value=$3

	echo "${content//$placeholder/$value}"

	unset content
	unset placeholder
	unset value
}

# Append in kubectl completion model, function for new command
# - In template resource file :
#   Set function name
#   Set last_command name
add_completion_configuration_part() {
	
	cmd_name=$1
	completion_type=$2
	model=$3

	command="${cmd_name##*kubectl-}"
	large_command="kubectl_$command"
	function_name="_$large_command"

	content=$(load_resource_template $completion_type)
	content=$(replace_placeholder "$content" "$FUNCTION_PLACEHOLDER" "$function_name")
	content=$(replace_placeholder "$content" "$KUBECTL_CMD_PLACEHOLDER" "$large_command")

	model=$(replace_placeholder "$model" "$CUSTOM_CMD_PLACEHOLDER" "$content\n$CUSTOM_CMD_PLACEHOLDER")

	echo -e "$model"

	unset cmd_name
	unset completion_type
	unset model
	unset command
	unset large_command
	unset function_name
	unset content
	unset model
}

# Add in Kubectl completion commands list, new command
# - Set alias for command : bash, watch...
add_completion_declaration_part() {
	
	cmd_name=$1
	completion_type=$2
	model=$3


	command="${cmd_name##*kubectl-}"
	command_insert="    commands+=(\"$command\")"

	model=$(replace_placeholder "$model" "$CUSTOM_CMD_DECLARATION" "$command_insert\n$CUSTOM_CMD_DECLARATION")

	echo -e "$model"

	unset cmd_name
	unset completion_type
	unset model
	unset command
	unset command_insert
}

# Add in switch case new command
# - A case exists for each type of completion (pod, node...)
add_bind_resource_command() {
	
	cmd_name=$1
	completion_type=$2
	model=$3

	command="${cmd_name##*kubectl-}"
	large_command="kubectl_$command"
	function_name="_$large_command"

	placeholder=$(find_k8s_resource_placeholder $completion_type)
	bind_declaration="| $large_command $placeholder"

	model=$(replace_placeholder "$model" "$placeholder" "$bind_declaration")

	echo -e "$model"

	unset cmd_name
	unset completion_type
	unset model
	unset command
	unset large_command
	unset function_name
	unset placeholder
	unset bind_declaration
}

# Load model completion file
# - Base model if it's first command addition
# - Temporary model, with previous commands additions
load_model_file() {

	model_file_name="kubectl-completion-model-$ZSH_KUBECTL_COMPLETION_MODEL"

	mkdir -p $PLUGIN_TEMP_DIR

	model=""

	if [ ! -f $TEMP_FILE_PATH ]; then
		model="$(cat $PLUGIN_COMPLETION_DIR/$model_file_name)"
	else
		model="$(cat $TEMP_FILE_PATH)"
	fi

	echo -e "$model"

	unset model
}

# Write overrided model in temp file
# - To be used by next command to add
write_model_file() {

	file_content=$1

	echo "$file_content" > $TEMP_FILE_PATH

	unset file_content
}


main() {

	cmd_name=$1
	completion_type=$2
	completion_model=$(load_model_file)

	command="${cmd_name##*kubectl-}"
	large_command="kubectl_$command"
	function_name="_$large_command"

	model="$completion_model"

	if [[ "$completion_type" != "none" ]]; then
		
		# Add function to describe command completion arguments/options
		model="$(add_completion_configuration_part $cmd_name $completion_type "$completion_model")"

		# Add command in type of completion switch case
		model="$(add_bind_resource_command $cmd_name $completion_type "$model")"
	fi

	# Add command in kubectl available commands list
	model="$(add_completion_declaration_part $cmd_name $completion_type "$model")"

	# Save completion model
	write_model_file "$model"

	unset cmd_name
	unset completion_type
	unset completion_model
	unset command
	unset large_command
	unset function_name
	unset model
}

main $1 $2