# Kubernetes Custom Plugins

Add custom Kubectl Commands in your terminal (bash/zsh support)

## Features

- Provides extra commands
- Offer possibility to add yours in commands folder
- Generate smart completion for all commands (basic and extra)
- Add [aliases](https://github.com/robbyrussell/oh-my-zsh/tree/master/plugins/kubectl) for kubectl (_extract from official zsh plugin_)

## Overview of custom commands

See at : https://gitlab.com/tcaro-public/kubectl-commands-repository

__You can submit merge requests to share your custom commands__

## Precondition

* Have kubectl with version __1.12__ minimum (_version which supports plugins discovering_)
* Have an _$HOME/bin_ folder in your $PATH (_create symlinks discovered by kubectl_)
* Have Git

## Installation for ZSH

1. Clone project in your $ZSH/custom/plugins folder
2. IF SSH clone : in path_to_plugin/.gitmodules, change url line by:<br/>	
`url = git@gitlab.com:tcaro-public/kubectl-custom-commands.git`
3. Run command to synchronize commands submodule:<br/>
`git submodule init && git submodule update --recursive`
4. Add it in zsh plugin list (_can be in conflict with official plugin_)
5. Restart your terminal (_or source ~/.zshrc_)

### How it works
1. Script is running only if completion file is not in your completion folder
2. Plugin add symbolic links for all commands
3. A completion file is generated and move in your completion folder
4. A command exists to force plugin rebuild<br/>
`kubectl_custom_rebuild`

### Load properly completion
1. Make sure you had a custom completion folder in your fpath<br/>
Edit .zshrc file `fpath=($ZSH/custom/completions $fpath)`
2. Enable custom completion<br/>
Edit .zshrc file `autoload -U compinit && compinit -u`
3. Change completion folder in kubectl-custom-commands.plugin.zsh<br/>if it not **$ZSH/custom/completions**


## Installation for BASH

1. Clone project
2. IF SSH clone : in path_to_plugin/.gitmodules file, change url line by:<br />`url = git@gitlab.com:tcaro-public/kubectl-custom-commands.git`
3. In your ~/.bashrc, add this line :<br/>`source <(sh [path_to]/kubectl-custom-commands/kubectl-custom-commands-bash.sh)`
4. Add this one if you have k alias :<br/>`source <(sh [path_to]/kubectl-custom-commands/kubectl-custom-commands-bash.sh | sed 's/kubectl/k/g')`

## Update plugin (and commands submodule)

* An alias exists to simplify update 
```bash
# cd in plugin folder, run some git commands, and back to previous folder
update_kubectl_custom_commands
```
* Or run your command to update folder (and submodule)

## Add your own custom commands

* Add your file in folder __commands__ (_a symlink will add in $HOME/bin_ and kubectl will add it like a plugin
> You can check with command `kubectl plugin list`

* Magic appends with filename pattern, so respect it

```bash
# kubectl-[command name]_[type of completion you want]
kubectl-[a-zA-Z0-9_]+(_completion-[pod|node|cluster|context|resource)?

# Exemple to create command : kubectl bash 
kubectl-bash_completion-pod
```
> If you want dashed command, use underscore in filename
```bash
# Exemple with dashed command : kubectl current-namespace
kubectl-current_namespace
```

* Classic completion is applied if no completion type is set in filename
> From official documentation : https://kubernetes.io/docs/tasks/extend-kubectl/kubectl-plugins/
> You can not extend official commands (get, delete...)
