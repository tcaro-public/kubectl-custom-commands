
# Export Variables
export ZSH_LOAD_PACKAGED_KUBECTL_COMMANDS=true
export ZSH_KUBECTL_CUSTOM_PLUGINS_DIR=$(dirname "$0")
export ZSH_KUBECTL_COMPLETION_MODEL='zsh'
export COMPLETION_FINAL_PATH="${ZSH}/custom/completions/_kubectl-custom-commands"

# Add custom Kubectl commands
if (( $+commands[kubectl] )); then
	
	CACHE_COMPLETION_FILE_PATH="${ZSH_CACHE_DIR}/kubectl_completion"

  if [[ ! -f "$COMPLETION_FINAL_PATH" ]]; then

  	# Add custom commands and create completions
  	${ZSH_KUBECTL_CUSTOM_PLUGINS_DIR}/scripts/add-kubectl-plugins.sh

  	if [[ -f "$CACHE_COMPLETION_FILE_PATH" ]]; then
  		cp $CACHE_COMPLETION_FILE_PATH $COMPLETION_FINAL_PATH
  	fi
  fi
fi

alias update_kubectl_custom_commands='cd $ZSH_KUBECTL_CUSTOM_PLUGINS_DIR; git pull --rebase; git submodule update --remote --merge; cd - > /dev/null'

alias kubectl_custom_rebuild='rm $COMPLETION_FINAL_PATH; source ~/.zshrc'

####
# EXTRACT FROM OFFICIAL ZSH KUBECTL PLUGIN
###

# This command is used a LOT both below and in daily life
alias k=kubectl

# Apply a YML file
alias kaf='kubectl apply -f'

# Drop into an interactive terminal on a container
alias keti='kubectl exec -ti'

# Manage configuration quickly to switch contexts between local, dev ad staging.
alias kcuc='kubectl config use-context'
alias kcsc='kubectl config set-context'
alias kcdc='kubectl config delete-context'
alias kccc='kubectl config current-context'

# General aliases
alias kdel='kubectl delete'
alias kdelf='kubectl delete -f'

# Pod management.
alias kgp='kubectl get pods'
alias kgpw='kgp --watch'
alias kgpwide='kgp -o wide'
alias kep='kubectl edit pods'
alias kdp='kubectl describe pods'
alias kdelp='kubectl delete pods'

# get pod by label: kgpl "app=myapp" -n myns
alias kgpl='kgp -l'

# Service management.
alias kgs='kubectl get svc'
alias kgsw='kgs --watch'
alias kgswide='kgs -o wide'
alias kes='kubectl edit svc'
alias kds='kubectl describe svc'
alias kdels='kubectl delete svc'

# Ingress management
alias kgi='kubectl get ingress'
alias kei='kubectl edit ingress'
alias kdi='kubectl describe ingress'
alias kdeli='kubectl delete ingress'

# Namespace management
alias kgns='kubectl get namespaces'
alias kens='kubectl edit namespace'
alias kdns='kubectl describe namespace'
alias kdelns='kubectl delete namespace'

# ConfigMap management
alias kgcm='kubectl get configmaps'
alias kecm='kubectl edit configmap'
alias kdcm='kubectl describe configmap'
alias kdelcm='kubectl delete configmap'

# Secret management
alias kgsec='kubectl get secret'
alias kdsec='kubectl describe secret'
alias kdelsec='kubectl delete secret'

# Deployment management.
alias kgd='kubectl get deployment'
alias kgdw='kgd --watch'
alias kgdwide='kgd -o wide'
alias ked='kubectl edit deployment'
alias kdd='kubectl describe deployment'
alias kdeld='kubectl delete deployment'
alias ksd='kubectl scale deployment'
alias krsd='kubectl rollout status deployment'

# Rollout management.
alias kgrs='kubectl get rs'
alias krh='kubectl rollout history'
alias kru='kubectl rollout undo'

# Port forwarding
alias kpf="kubectl port-forward"

# Tools for accessing all information
alias kga='kubectl get all'
alias kgaa='kubectl get all --all-namespaces'

# Logs
alias kl='kubectl logs'
alias klf='kubectl logs -f'

# File copy
alias kcp='kubectl cp'

# Node Management
alias kgno='kubectl get nodes'
alias keno='kubectl edit node'
alias kdno='kubectl describe node'
alias kdelno='kubectl delete node'
